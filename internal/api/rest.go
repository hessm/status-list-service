package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"sync"
)

var server *gin.Engine

func init() {
	server = gin.Default()
	server.Use(errorHandler())
}

func errorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if len(c.Errors) > 0 {
			c.JSON(-1, gin.H{
				"error": c.Errors[0].Error(),
			})
		}

	}
}

func defineEndpoints() {
	server.POST("/v1/:tenantId/revoke/:blockId/:index", handleRevoke)
}

func handleRevoke(ctx *gin.Context) {
	tenantId := ctx.Param("tenantId")
	blockId, err := strconv.Atoi(ctx.Param("blockId"))
	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	index, err := strconv.Atoi(ctx.Param("index"))
	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	err = db.RevokeCredentialInSpecifiedBlock(ctx, tenantId, blockId, index)
	if err != nil {
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"tenantId": tenantId,
		"blockId":  blockId,
		"index":    index,
		"status":   "revoked",
	})
}

func startRest(port int, wg *sync.WaitGroup) {
	defer wg.Done()

	defineEndpoints()
	err := server.Run(fmt.Sprintf(":%d", port))
	if err != nil {
		panic(err)
	}
}
