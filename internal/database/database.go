package database

import (
	"context"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/entity"
)

type DbConnection interface {
	AllocateIndexInCurrentBlock(ctx context.Context, tenantId string) (*entity.StatusData, error)
	RevokeCredentialInSpecifiedBlock(ctx context.Context, tenantId string, blockId int, index int) error
	CreateTableForTenantIdIfNotExists(ctx context.Context, tenantId string) error
	Close()
}

// TablePrefix is needed for table name cause of postgres name convention -> no integers allowed
const TablePrefix = "tenant_id_"

type Database struct {
	DbConnection
}

func New(username string, password string, host string, port int, db string, blockSizeInBytes int) (*Database, error) {
	dbConnection, err := newPostgresConnection(username, password, host, port, db, blockSizeInBytes)
	return &Database{DbConnection: dbConnection}, err
}
