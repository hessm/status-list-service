package entity

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestNewBlockWithCorrectSizeAndFreeCount(t *testing.T) {
	wantedByteSize := 100
	wantedFreeSize := wantedByteSize * 8

	newBlock := NewBlock(wantedByteSize)

	require.Equal(t, wantedByteSize, len(newBlock.Block))
	require.Equal(t, wantedFreeSize, newBlock.Free)
}

func TestRevokeIndexInBlock(t *testing.T) {
	byteBlockSize := 2

	wantedBlock := make([]byte, byteBlockSize)
	for i := range wantedBlock {
		if i == 0 {
			wantedBlock[i] ^= 254
		} else {
			wantedBlock[i] ^= 255
		}
	}

	block := NewBlock(byteBlockSize)
	block.RevokeAtIndex(7)

	require.Equal(t, wantedBlock, block.Block)
}

func TestGetNextFreeIndexAndReduceFreeCount(t *testing.T) {
	byteBlockSize := 10
	maxIndex := (byteBlockSize * 8) - 1

	block := NewBlock(byteBlockSize)
	for i := 0; i < maxIndex; i++ {
		_, err := block.AllocateNextFreeIndex()
		if err != nil {
			t.Errorf("unexpected error occured: %v", err)
		}
	}

	wantedIndex := maxIndex
	wantedFreeCount := 0
	index, err := block.AllocateNextFreeIndex()
	if err != nil {
		t.Errorf("unexpected error occured: %v", err)
	}

	require.Equal(t, wantedIndex, index)
	require.Equal(t, wantedFreeCount, block.Free)
}

func TestTryToAllocateNewIndexInFullyAllocatedBlock(t *testing.T) {
	byteBlockSize := 10
	maxIndex := (byteBlockSize * 8) - 1

	block := NewBlock(byteBlockSize)
	for i := 0; i <= maxIndex; i++ {
		_, err := block.AllocateNextFreeIndex()
		if err != nil {
			t.Errorf("unexpected error occured: %v", err)
		}
	}

	_, err := block.AllocateNextFreeIndex()
	require.Error(t, ErrFullyAllocated, err)
}
