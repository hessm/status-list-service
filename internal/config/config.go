package config

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"strings"
)

type statusListConfiguration struct {
	LogLevel string `mapstructure:"logLevel"`
	Port     int    `mapstructure:"servingPort"`
	Database struct {
		Username string `mapstructure:"username"`
		Password string `mapstructure:"password"`
		Host     string `mapstructure:"host"`
		Port     int    `mapstructure:"port"`
		Db       string `mapstructure:"db"`
	} `mapstructure:"database"`
	CreationTopic    string `mapstructure:"creationTopic"`
	BlockSizeInBytes int    `mapstructure:"blockSizeInBytes"`
}

var CurrentStatusListConfig statusListConfiguration

func LoadConfig() error {
	setDefaults()
	readConfig()

	if err := viper.Unmarshal(&CurrentStatusListConfig); err != nil {
		return err
	}
	setLogLevel()
	return nil
}

func readConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	viper.SetEnvPrefix("STATUSLIST")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if errors.As(err, &configFileNotFoundError) {
			log.Printf("Configuration not found but environment variables will be taken into account.")
		}
	}
	viper.AutomaticEnv()
}

func setDefaults() {
	viper.SetDefault("logLevel", "info")
	viper.SetDefault("servingPort", 8080)
	viper.SetDefault("databaseUrl", "postgres://root:pass@postgres_db:5432/0")
	viper.SetDefault("creationTopic", "status")
}

func setLogLevel() {
	var logLevel log.Level
	switch strings.ToLower(CurrentStatusListConfig.LogLevel) {
	case "trace":
		logLevel = log.TraceLevel
	case "debug":
		logLevel = log.DebugLevel
	case "info":
		logLevel = log.InfoLevel
	case "error":
		logLevel = log.ErrorLevel
	case "fatal":
		logLevel = log.FatalLevel
	case "panic":
		logLevel = log.PanicLevel
	default:
		logLevel = log.WarnLevel
	}
	log.SetLevel(logLevel)
	log.Infof("loglevel set to %s", logLevel.String())
}
