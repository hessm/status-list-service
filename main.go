package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/api"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/database"
)

func main() {
	if err := config.LoadConfig(); err != nil {
		log.Error(err)
	}
	currentConf := &config.CurrentStatusListConfig

	dbConf := &currentConf.Database
	db, err := database.New(
		dbConf.Username,
		dbConf.Password,
		dbConf.Host,
		dbConf.Port,
		dbConf.Db,
		currentConf.BlockSizeInBytes,
	)
	if err != nil {
		log.Error(err)
	}
	defer db.Close()

	api.Listen(db, currentConf.Port, currentConf.CreationTopic)
}
